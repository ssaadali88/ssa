﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SSA.Startup))]
namespace SSA
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
