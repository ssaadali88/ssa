﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SSA
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);

            routes.MapRoute(
                "Default",
                "{controller}/{action}",
                new { controller = "Home", action = "Index" }
                );

            routes.MapRoute(null,
              "{controller}/{action}/{page}",
              new
              {
                  controller = "Category",
                  action = "Index",
                  cat = (string)null
              },
              new
              {
                  page = @"\d+"
              }
              );

            routes.MapRoute(null,
            "{controller}/{action}/{searchType}/{CAT}",
            new
            {
                controller = "Category",
                action = "Index",
                page = 1
            },
            new
            {
                page = @"\d+"
            }
            );

            routes.MapRoute(null,
        "{controller}/{action}/{searchType}/{CAT}/{page}", // Matches /Football/Page567
        new
        {
            controller = "Category",
            action = "Index"
        }, // Defaults
        new
        {
            page = @"\d+"
        } // Constraints: page must be numerical
        );
            //----------------------------------------------------

            routes.MapRoute(null,
          "{controller}/{action}/{searchType}",
          new
          {
              controller = "Category",
              action = "Index",
              page = 1
          },
          new
          {
              page = @"\d+"
          }
          );

            routes.MapRoute(null,
        "{controller}/{action}/{searchType}/{page}", // Matches /Football/Page567
        new
        {
            controller = "Category",
            action = "Index"
        }, // Defaults
        new
        {
            page = @"\d+"
        } // Constraints: page must be numerical
        );
        }
    }
}
