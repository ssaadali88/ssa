﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSA.Models
{
    public class IStoreRepository
    {
        IQueryable<Store> Store { get; }
        IQueryable<ProductCategoryWiseStoreView> ProductCategoryWiseStore { get; }
        IQueryable<ProductCategory> ProductCategory { get; }
    }
}