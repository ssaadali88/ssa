﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq;
using System.Web;

namespace SSA.Models
{
    public class SQLStoreRepository : IStoreRepository
    {
        private Table<Store> storeTable;
        private Table<ProductCategoryWiseStoreView> productCategoryWiseStoreTable;
        private Table<ProductCategory> productCategoryTable;

        public SQLStoreRepository(string ConnectionString)
        {
            DataContext S = new DataContext(ConnectionString);

            storeTable = S.GetTable<Store>();
            productCategoryWiseStoreTable = S.GetTable<ProductCategoryWiseStoreView>();
            productCategoryTable = S.GetTable<ProductCategory>();
        }

        public IQueryable<Store> Store
        {
            get { return storeTable; }
        }

        public IQueryable<ProductCategoryWiseStoreView> ProductCategoryWiseStore
        {
            get { return productCategoryWiseStoreTable; }
        }

        public IQueryable<ProductCategory> ProductCategory
        {
            get { return productCategoryTable; }
        }
    }
}