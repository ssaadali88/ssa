﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSA.Models
{
    public class StoreListViewModel
    {
        public IEnumerable<ProductCategoryWiseStoreView> ProductCategoryWiseStoreSearch
        {
            get;
            set;
        }

        public PagingInfo2 PagingInfo
        {
            get;
            set;
        }

        public string CurrentCategory
        {
            get;
            set;
        }


    }
}