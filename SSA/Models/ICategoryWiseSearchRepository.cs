﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSA.Models
{
    public class ICategoryWiseSearchRepository
    {
        IQueryable<Product> Products { get; }
        IQueryable<ProductCategory> ProductCategory { get; }
        IQueryable<Store> Store { get; }

        IQueryable<ProductSearchView> ProductSearch { get; }

        IQueryable<CatWPView> CatWP { get; }
       
        IQueryable<ProductCategoryWiseStoreView> ProductCategoryWiseStore { get; }
        
    }
}