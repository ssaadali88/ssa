﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Web;

namespace SSA.Models
{
    public class SQLProductRepository : IProductRepository
    {
        private Table<Product> productsTable;
        private Table<ProductCategory> productCategoryTable;
        private Table<Store> storeTable;

        private Table<ProductSearchView> productSearchTable;

        private Table<CatWPView> catWPTable;

        public SQLProductRepository(string ConnectionString)
        {
            DataContext A = new DataContext(ConnectionString);

            productsTable = A.GetTable<Product>();
            productCategoryTable = A.GetTable<ProductCategory>();
            storeTable = A.GetTable<Store>();

            productSearchTable = A.GetTable<ProductSearchView>();

            catWPTable = A.GetTable<CatWPView>();
        }

        public IQueryable<Product> Products
        {
            get { return productsTable; }
        }

        public IQueryable<ProductCategory> ProductCategory
        {
            get { return productCategoryTable; }
        }

        public IQueryable<Store> Store
        {
            get { return storeTable; }
        }

        public IQueryable<ProductSearchView> ProductSearch
        {
            get { return productSearchTable; }
        }

        public IQueryable<CatWPView> CatWP
        {
            get { return catWPTable; }
        }

    }
}