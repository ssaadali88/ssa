﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SSA.Models
{
    public class CategoryWiseSearchListViewModel
    {
        public IEnumerable<ProductSearchView> ProductSearch
        {
            get;
            set;
        }

        public IEnumerable<ProductCategoryWiseStoreView> ProductCategoryWiseStoreSearch
        {
            get;
            set;
        }

        public PagingInfo2 PagingInfo
        {
            get;
            set;
        }

        public string CurrentCategory
        {
            get;
            set;
        }

        public string SearchType
        {
            get;
            set;
        }

    }
}