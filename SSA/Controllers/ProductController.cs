﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSA.Models;
using System.Configuration;

namespace SSA.Controllers
{
    public class ProductController : Controller
    {
        private SQLProductRepository productRepository;

        public ProductController()
        {
            string connString = ConfigurationManager.ConnectionStrings["SSAConnectionString1"].ToString();
            productRepository = new SQLProductRepository(connString);
        }

        public ActionResult ProductCategoryList()
        {
            var ProductCategory = productRepository.ProductCategory.ToList();
            return View(ProductCategory);
        }

        public ActionResult CategoryWiseProduct()
        {
            var CatWP = productRepository.CatWP.ToList();
            return View(CatWP);
        }

        public ActionResult SearchProduct(string CAT, int page = 1)
        {
           int pageSize = 12;
            if (page <= 1)
            {
                page = 1;
            }
            ProductListViewModel viewModel = new ProductListViewModel
            {
                ProductSearch = productRepository.ProductSearch
                .Where(p => CAT == null ? true : p.CategoryName == CAT)
                .OrderBy(p => p.ProductId)
                .Skip((page - 1) * pageSize)
                .Take(pageSize),

                PagingInfo = new PagingInfo2
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    NextPage = page + 1,
                    PreviousPage = page - 1,
                    TotalItems = productRepository.ProductSearch.Where(p => CAT == null ? true : p.CategoryName == CAT).Count()
                },

                CurrentCategory = CAT
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult SearchProduct(string name = "")
        {
            ProductListViewModel viewModel = new ProductListViewModel();
            
            if (!string.IsNullOrEmpty(name))
            {
                viewModel.ProductSearch = productRepository.ProductSearch
                    .Where(s => s.ProductName.Contains(name)
                    || s.CategoryName.Contains(name));
            }

            return View(viewModel);

        }
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
    }
}