﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SSA.Models;
using System.Configuration;
namespace SSA.Controllers
{
    public class CategoryController : Controller
    {
        private SQLCategoryWiseSearchRepository categoryWiseSearchRepository;

        public CategoryController()
        {
            string connString = ConfigurationManager.ConnectionStrings["SSAConnectionString1"].ToString();
            categoryWiseSearchRepository = new SQLCategoryWiseSearchRepository(connString);
        }

        // GET: Category
        public ActionResult Index(string CAT, string searchType, int page = 1)
        {
            if (searchType != null)
            {
                TempData["SearchType"] = searchType;
            }
           
            int pageSize = 12;
            if (page <= 1)
            {
                page = 1;
            }

            CategoryWiseSearchListViewModel viewModel = new CategoryWiseSearchListViewModel();

            if (searchType == "Products" || searchType == null)
            {
                viewModel = new CategoryWiseSearchListViewModel
                {
                    ProductSearch = categoryWiseSearchRepository.ProductSearch
            .Where(p => CAT == null ? true : p.CategoryName == CAT)
            .OrderBy(p => p.ProductId)
            .Skip((page - 1) * pageSize)
            .Take(pageSize),

                    PagingInfo = new PagingInfo2
                    {
                        CurrentPage = page,
                        ItemsPerPage = pageSize,
                        NextPage = page + 1,
                        PreviousPage = page - 1,
                        TotalItems = categoryWiseSearchRepository.ProductSearch.Where(p => CAT == null ? true : p.CategoryName == CAT).Count()
                    },

                    CurrentCategory = CAT,
                    SearchType = searchType
                };
            }
            else if (searchType == "Stores")
            {
                viewModel = new CategoryWiseSearchListViewModel
                {
                    ProductCategoryWiseStoreSearch = categoryWiseSearchRepository.ProductCategoryWiseStore
            .Where(p => CAT == null ? true : p.CategoryName == CAT)
            .OrderBy(p => p.StoreId)
            .Skip((page - 1) * pageSize)
            .Take(pageSize),

                    PagingInfo = new PagingInfo2
                    {
                        CurrentPage = page,
                        ItemsPerPage = pageSize,
                        NextPage = page + 1,
                        PreviousPage = page - 1,
                        TotalItems = categoryWiseSearchRepository.ProductCategoryWiseStore.Where(p => CAT == null ? true : p.CategoryName == CAT).Count()
                    },

                    CurrentCategory = CAT,
                    SearchType = searchType
                };
            }
            
            return View(viewModel);
        }

        public ActionResult CategoryWiseSearch()
        {
            var CatWP = categoryWiseSearchRepository.CatWP.ToList();
            return View(CatWP);
        }

        public ActionResult Products(string CAT, int page = 1)
        {
            return View();
        }

        public ActionResult Stores(string CAT, int page = 1)
        {
            return View();
        }
    }
}